text='beautiful is better than ugly'
print(text.split())
print(text.split(maxsplit=1))
print(text.rsplit(maxsplit=2))
print('1,2,3,4,5'.split(','))
print(','.join(['1', '2', '3', '4', '5']))
print(':'.join(map(str,range(1,6))))
print(list(map(str,range(1,6))))
print(map(str,range(1,5)))
print(''.join((map(str,range(1,10)))))

ljx='Explicit is better that import'
print(ljx.lower())
print(ljx.upper())
print(ljx.capitalize())
print(ljx.title())
print(ljx.swapcase())

l='Simple is better than complex.'
print(l.startswith('simple'))
print(l.startswith('Simple'))
print(l.endswith('ljx'))
print(l.endswith('complex'))
print(l.endswith('.'))

text='  =======ljx==== ####   '
print(text.strip())
print(text.strip('=# '))

#输如一个字符串，删除其中的重复空格，保留字符串之间的一个空格，然后整理输出完整的字符串
ljx=input('请输入一个包含空格的字符串：')
print(' '.join(ljx.split()))


#输入一些字母，将元音字母 a e i o u 替换成大写字母
ljx=input('请输入一个字母字符串：')
l=''.maketrans('a,e,i,o,u','A,E,I,O,U')
print(ljx.translate(l))

s1=input('请输入一个字符串：')
s2=s1[::-1]
if s2==s1:
    print('yes')
else:
    print('no')

s1=input('请输入一个字符串：')
s2=''.join(map(str,list(s1)))
if s2==s1:
    print('yes')

else:
    print('no')


