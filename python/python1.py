sock={'ljx':56,'jk':94,'hfi':58,'kf':55}
print(sock)
sock['ljx']=914
print(sock)
sock['lx']=56
print(sock)

lj={'ip':'127.0.0.1','popt':80}
lj.update({'ip':'192.168.10.10','popp':8896})
print(lj)
print(lj.pop('ip'))
print(lj.popitem())
del lj['popt']
print(lj)
print(lj)

#编写程序，首先生成包含1000个水机数字字符的字符串，然后统计每个数字的出现次数
from string import digits
from random import choice
z=''.join(choice(digits) for i in range(1000))
result={}
for ch in z:
    result[ch]=result.get(ch,0)+1
for digit,fre in sorted(result.items()):
    print(digit,fre,sep=':')

data={30,45,10}
data.add(20)
print(data)
data.add(45)
print(data)
data.update({30,60})
print(data)

book='python高级运用'
print(book.encode()) #转化为编码格式
print(book.encode().decode())   #使用decode()函数解码，转换为字符串
print(book.encode('gbk'))    #使用gbk编码格式
print(book.encode('gbk').decode('gbk'))

print('{0:.4f}'.format(10/3)) #结果转换为浮点数，保留4位小数
print('(0:.2%)'.format(1/3))  #结果转化为百分数，保留两位小数
print('{0:>10.2%}'.format(1/3)) #结果靠右边对齐，总长为10，为百分数，保留两位小鼠
print("{0:,} in hex is: {0:#x}, in oct is {0:#o}".format(5555555)) #o表示八进制数，#x表示格式化十六进制
print("{1} in hex is : {1:#x}, {0} in oct is {0:o}".format(6666,666666))



tex='仍是雨夜，凝望窗外，沉默的天际，问苍天，可会知明天该如何'
print(tex.index('夜'))
print(tex.rindex('天'))
print(tex.count('天'))

text="python是一门非常好的编程语言"
print(tex.replace('好','优雅').replace('编程','程序设计'))
print(text)

table=''.maketrans('0123456789','零一二三四五六七八九') #使用maketrans函数生成字符映射列表
print('LJX:30647259'.translate(table))  #translate函数可以把映射表的对应字符转换为字符串

print('左'.ljust(25)+'右')
print('左'.ljust(25,'*')+'右')
print('左'.rjust(25,'%')+'右')
print('LJX'.center(25,'-'))