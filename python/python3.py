ljx = input("请输入一个字符串:")
# 使用join函数直接连接，使用内置函数reversed()翻转ljx转化为列表，再转换为字符串
# 使用map函数返回迭代对象
l = ''.join(map(str, list(reversed(ljx))))
if (ljx == l):  # 条件判断
    print('True')
else:
    print('False')


# -*- coding: utf-8 -*-
def digitSum(n):
    if n == 0:
        return 0
    return digitSum(n // 10) + n % 10


# 位置参数
def add(a, b):
    print("a={0},b={1}".format(a, b))
    return a + b


print(add(3, 5))


# 默认值参数
def add(a, b=8):
    print("a={0},b={1}".format(a, b))
    return a * b


print(add(5, 6))
print(add(4))


# 关键参数
def add(a=10, b=2):
    print("a={0},b={1}".format(a, b))
    return a * b


print(add(b=10, a=2))


# 可变长度参数
def add(a, b, *lj, **ljx):
    print('a={0},b={1},lj={2},ljx={3}'.format(a, b, lj, ljx))
    return a + b + sum(lj) + sum(ljx.values())


print(add(3, 8, 1, 2, 3, 4, x=5, y=6, z=7))


def mul(*mult):
    m = 1
    for n in mult:
        m = m * n
    return m


print(mul(2, 5, 8, 6))


def func():
    global x
    x = 666
    y = 888
    print(x, y, z, sep=',')


x, y, z = [3, 5, 7]
func()
print(x, y, z, sep=',')